#include "Adafruit_VL53L0X.h"
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// Light Distance Sensor
Adafruit_VL53L0X lox = Adafruit_VL53L0X();

// Wifi data
const char* ssid = "iotempire-CooleBoys";
const char* password = "abcde12356";
const char* mqtt_server = "192.168.12.1";
WiFiClient espClient;

// MQTT data
PubSubClient client;

void setup() {
  Serial.begin(115200);
  setup_wifi();
  client.setClient(espClient);
  client.setServer(mqtt_server, 1883);
  //client.setCallback(callback);
  
  // wait until serial port opens for native USB devices
  while (! Serial) {
    delay(1);
  }
  
  if (!lox.begin()) {
    Serial.println(F("Failed to boot VL53L0X"));
    while(1);
  }
  // power 
  Serial.println(F("LightSensor Range: \n\n")); 
}

void loop() {
 sensorLoop();
 // Network
  if (!client.connected()) {
    reconnect();
  }

  client.loop();
  
  //wait 300 ms before calculating again
  delay(300);
}



void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    //Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266Client")) {
      //Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("sensor/state", "connected");
      // ... and resubscribe
      client.subscribe("integrator/sensor/state");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void sensorLoop(){
 VL53L0X_RangingMeasurementData_t measure;
    long distance;
  lox.rangingTest(&measure, false); // pass in 'true' to get debug data printout!

  if (measure.RangeStatus != 4) {  // phase failures have incorrect data
    //Serial.print("Distance (mm): "); Serial.println(measure.RangeMilliMeter);
    char str[21];
    distance = measure.RangeMilliMeter;
    sprintf(str, "%d", distance);
    client.publish("sensor/data/distance/light", str);
    Serial.print(distance);
    Serial.println(" mm");
  } else {
    Serial.println(" out of range ");
  }
}


