#include <ESP8266WiFi.h>
#include <PubSubClient.h>

//PINMAPPING
//accoustic distance
#define triggerPin 16
#define echoPin 14

//raindrop
// lowest and highest sensor readings:
#define sensorMin 0     // sensor minimum
#define sensorMax 1024 // sensor maximum

// Wifi data
const char* ssid = "iotempire-CooleBoys";
const char* password = "abcde12356";
const char* mqtt_server = "192.168.12.1";
WiFiClient espClient;

// MQTT data
PubSubClient client;

void setup() {
  Serial.begin (115200);
  setup_wifi();
  client.setClient(espClient);
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  distanceSensorSetup();
}

void loop() {
  distanceSensorLoop();
  raindropSensorLoop();

  // Network
  if (!client.connected()) {
    reconnect();
  }

  client.loop();
  
  //wait 300 ms before calculating again
  delay(300);
}

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266Client")) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("sensor/state", "connected");
      // ... and resubscribe
      client.subscribe("integrator/sensor/state");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void distanceSensorSetup(){
  pinMode(triggerPin, OUTPUT);
  pinMode(echoPin, INPUT);
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
}

void distanceSensorLoop(){
  long duration, distance;

  //set trigger to LOW
  digitalWrite(triggerPin, LOW); 
  delayMicroseconds(2); 
  //set trigger to HIGH
  digitalWrite(triggerPin, HIGH);
  
  delayMicroseconds(10); 
  digitalWrite(triggerPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = (duration/2) / 29.1;

  //print results
  char str[21];
  sprintf(str, "%d", distance);
  client.publish("sensor/data/distance/acoustic", str);
  Serial.print(distance);
  Serial.println(" cm");
}

void raindropSensorLoop(){
  // read the sensor on analog D8:
  int sensorReading = analogRead(15);

    Serial.print(sensorReading);
  Serial.println(" rain");
  
  // map the sensor range (four options):
  // ex: 'long int map(long int, long int, long int, long int, long int)'
  int range = map(sensorReading, sensorMin, sensorMax, 0, 3);

  // range value:
  switch (range) {
    case 0:    // Sensor getting wet
      Serial.println("Pretty wet out there...");
      client.publish("sensor/data/rain", "heavy");
    break;
    case 1:    // Sensor getting wet
      Serial.println("Light Rain");
      client.publish("sensor/data/rain", "light");
    break;
  }
}


