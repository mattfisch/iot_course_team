/*
 Basic ESP8266 MQTT example

 This sketch demonstrates the capabilities of the pubsub library in combination
 with the ESP8266 board/library.

 It connects to an MQTT server then:
  - publishes "hello world" to the topic "outTopic" every two seconds
  - subscribes to the topic "inTopic", printing out any messages
    it receives. NB - it assumes the received payloads are strings not binary
  - If the first character of the topic "inTopic" is an 1, switch ON the ESP Led,
    else switch it off

 It will reconnect to the server if the connection is lost using a blocking
 reconnect function. See the 'mqtt_reconnect_nonblocking' example for how to
 achieve the same result without blocking the main loop.

 To install the ESP8266 board, (using Arduino 1.6.4+):
  - Add the following 3rd party board manager under "File -> Preferences -> Additional Boards Manager URLs":
       http://arduino.esp8266.com/stable/package_esp8266com_index.json
  - Open the "Tools -> Board -> Board Manager" and click install for the ESP8266"
  - Select your ESP8266 in "Tools -> Board"

*/

#include <Wire.h> 
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <LiquidCrystal_I2C.h>

// PINS
#define buttonPin 5
#define lcdSda 4
#define lcdScl 0

// Set the LCD address to 0x27 for a 16 chars and 2 line display
LiquidCrystal_I2C lcd(0x27, 16, 2);

// Wifi data
const char* ssid = "iotempire-CooleBoys";
const char* password = "abcde12356";
const char* mqtt_server = "192.168.12.1";
WiFiClient espClient;

// MQTT data
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;

// Topic data
// const char* outTopic = "control";
const char* inTopic = "integrator/dataAvailable";

// Control states
char* state = "off";
char* offState = "off";
char* onState = "on";

// Button states
int buttonState = 0;
int buttonFlag = 1;

// Sensor Values
String valOptic = "NaN";
String valAcoustic = "Nan";

void setup() {
  // initialize the pushbutton pin as input:
  pinMode(buttonPin, INPUT);

  // Initialize the BUILTIN_LED pin as an output
  pinMode(LED_BUILTIN, OUTPUT);   

  // Initialize serial monitor
  Serial.begin(115200);

  // Initialize wifi
  setup_wifi();

  // Initialize MQTT
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

  // Initialize the LCD
  lcd.begin(lcdSda, lcdScl);  // sda=4, scl=0
  lcd.backlight();
  lcd.clear();
  lcd.print("Ready");
}

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  String data = "";
  for (int i = 0; i < length; i++) {
    data += (char)payload[i];
  }

  String type = getValue(data, '/', 0);
  String value = getValue(data, '/', 1);

  if (type.equals("optic")) {
    valOptic = value;
  } else if (type.equals("acoustic")) {
    valAcoustic = value;
  }

  formatLcd();
}

void formatLcd() {
  String text = "";
  lcd.clear();

  text += "OPTI: " + valOptic;
  while (text.length() < 16) {
    text += " ";
  }
  printString(text);
  lcd.print(text);

  text = "";
  text += "SR04: " + valAcoustic;
  printString(text);
  lcd.print(text);
}

void printString(String text){
  Serial.print("[");
  Serial.print(text);
  Serial.println("]");
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266Client")) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      // client.publish(outTopic, offState);
      // ... and resubscribe
      client.subscribe(inTopic);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

// https://stackoverflow.com/questions/9072320/split-string-into-string-array
String getValue(String data, char separator, int index)
{
  int found = 0;
  int strIndex[] = {0, -1};
  int maxIndex = data.length()-1;

  for(int i=0; i<=maxIndex && found<=index; i++){
    if(data.charAt(i)==separator || i==maxIndex){
        found++;
        strIndex[0] = strIndex[1]+1;
        strIndex[1] = (i == maxIndex) ? i+1 : i;
    }
  }

  return found>index ? data.substring(strIndex[0], strIndex[1]) : "";
}

void loop() {

  // Network
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  // Button
  buttonState = digitalRead(buttonPin);
  if (buttonState == HIGH) {
    // not pressed
    // Serial.println("Button: HIGH");
    buttonFlag = 1;
  } else {
    // pressed
    if (buttonFlag == 1) {
      // Serial.println("LOW");
      // Handle state changes
      if (state == onState) {
        state = offState;
        // client.publish(outTopic, state);
        lcd.clear();
        lcd.print("Off");
      } else if (state == offState){
        state = onState;
        lcd.clear();
        lcd.print("Receiving Values");
        // client.publish(outTopic, state);
      }

      // Publish state changes
      Serial.println(state);
    }
    buttonFlag = 0;
  }

  delay(50);
}
