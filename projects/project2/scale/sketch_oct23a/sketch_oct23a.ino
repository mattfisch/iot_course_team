#include <HX711.h>
HX711 hx(9, 10, 128, 0.002155172);

void setup() {
  Serial.begin(9600);
}

void loop() {
  delay(500);
  double sum = 0;
  for (int i = 0; i < 10; i++)
    sum += hx.read();
  Serial.println(sum/10);
}
