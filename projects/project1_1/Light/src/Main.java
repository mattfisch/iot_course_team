import org.fusesource.mqtt.client.BlockingConnection;
import org.fusesource.mqtt.client.MQTT;
import org.fusesource.mqtt.client.QoS;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Random;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

public class Main {
    static boolean currentLightValue = true;

    public static void main(String[] args) throws IOException {

        MQTT mqtt = new MQTT();
        try {
            mqtt.setHost("192.168.12.1", 1883);
        } catch (
                URISyntaxException _e) {
            _e.printStackTrace();
        }

        BlockingConnection connection = mqtt.blockingConnection();

        try {
            connection.connect();
        } catch (
                Exception _e) {
            _e.printStackTrace();
        }

        while (true) {

            try {
                connection.publish("light", (currentLightValue ? String.valueOf(1).getBytes() : String.valueOf(0).getBytes()), QoS.AT_LEAST_ONCE, false);
            } catch (
                    Exception _e) {
                _e.printStackTrace();
            }

            System.out.println("Current Light Value: " + (currentLightValue ? "day" : "night"));

            System.in.read();

            currentLightValue = !currentLightValue;

        }
    }
}
