load('api_config.js');
load('api_gpio.js');
load('api_mqtt.js');
load('api_timer.js');
load('api_net.js');
load('api_sys.js');
load('api_neopixel.js');

let led = Cfg.get('pins.led');

//flag, if rave is currently on or off
let currentRaveMode = 0;

//pin, where the led is connected
let pin = 2;

//number of pixels on the pixel strip
let numPixels = 8;
let colorOrder = NeoPixel.RGB;

//init and reset LED-Strip
let strip = NeoPixel.create(pin, numPixels, colorOrder);
strip.clear();
strip.show();

//currently active pixel
let cnt1 = 0;
let cnt2 = 7;
let discoColorCntRed1 = 0;
let discoColorCntGreen1 = 10;
let discoColorCntBlue1 = 0;
let discoColorCntRed2 = 0;
let discoColorCntGreen2 = 10;
let discoColorCntBlue2 = 0;
let raveTimerId1;
let raveTimerId2;

// Blink built-in LED every second
GPIO.set_mode(led, GPIO.MODE_OUTPUT);
Timer.set(1000, true, function() {
  let value = GPIO.toggle(led);
}, null);

// Monitor network connectivity.
Net.setStatusEventHandler(function(ev, arg) {
  let evs = '???';
  if (ev === Net.STATUS_DISCONNECTED) {
    evs = 'DISCONNECTED';
  } else if (ev === Net.STATUS_CONNECTING) {
    evs = 'CONNECTING';
  } else if (ev === Net.STATUS_CONNECTED) {
    evs = 'CONNECTED';
  } else if (ev === Net.STATUS_GOT_IP) {
    evs = 'GOT_IP';
  }
  print('== Net event:', ev, evs);
}, null);

MQTT.sub('control', function(conn, topic, msg) {
  print('Topic:', topic, 'message:', msg);
  if(topic === 'control'){
    toggle_rave(msg);
  }
}, null);

function toggle_rave(msg){
  if(msg === 'on' && currentRaveMode === 0){
        print("rave on");
        currentRaveMode = 1;
        raveTimerId1 = Timer.set(20, true, function() {
          strip.clear();
          strip.setPixel(++cnt1 % 8,discoColorCntRed1++%10, --discoColorCntGreen1, discoColorCntBlue1++%5);
          strip.setPixel(cnt2, discoColorCntRed2, discoColorCntGreen1, discoColorCntBlue2);
          strip.show();
          if(discoColorCntGreen1 === 1){
            discoColorCntGreen1 = 10;
          }
        }, null);
         raveTimerId2 = Timer.set(50, true, function() {
          strip.clear();
          strip.setPixel(cnt2-=2,discoColorCntRed2++%10, --discoColorCntGreen2, discoColorCntBlue2++%5);
          strip.show();
          if(discoColorCntGreen2 === 1){
            discoColorCntGreen2 = 10;
          }
           if(cnt2 === 1){
            cnt2 = 7;
          }
        }, null);
  }else if(msg === 'off' && currentRaveMode === 1){
      print("rave off");
      Timer.del(raveTimerId1);
      Timer.del(raveTimerId2);
      strip.clear();
      strip.show();
      currentRaveMode = 0;
  }
}
