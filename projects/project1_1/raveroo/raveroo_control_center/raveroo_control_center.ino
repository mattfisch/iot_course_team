/*
 Basic ESP8266 MQTT example

 This sketch demonstrates the capabilities of the pubsub library in combination
 with the ESP8266 board/library.

 It connects to an MQTT server then:
  - publishes "hello world" to the topic "outTopic" every two seconds
  - subscribes to the topic "inTopic", printing out any messages
    it receives. NB - it assumes the received payloads are strings not binary
  - If the first character of the topic "inTopic" is an 1, switch ON the ESP Led,
    else switch it off

 It will reconnect to the server if the connection is lost using a blocking
 reconnect function. See the 'mqtt_reconnect_nonblocking' example for how to
 achieve the same result without blocking the main loop.

 To install the ESP8266 board, (using Arduino 1.6.4+):
  - Add the following 3rd party board manager under "File -> Preferences -> Additional Boards Manager URLs":
       http://arduino.esp8266.com/stable/package_esp8266com_index.json
  - Open the "Tools -> Board -> Board Manager" and click install for the ESP8266"
  - Select your ESP8266 in "Tools -> Board"

*/

#include <Wire.h> 
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <LiquidCrystal_I2C.h>

// Set the LCD address to 0x27 for a 16 chars and 2 line display
LiquidCrystal_I2C lcd(0x27, 16, 2);

// Wifi data
const char* ssid = "iotempire-CooleBoys";
const char* password = "abcde12356";
const char* mqtt_server = "192.168.12.1";
WiFiClient espClient;

// MQTT data
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;

// Topic data
const char* outTopic = "control";
const char* inTopic = "light";

// Control states
char* state = "off";

char* offState = "off";
char* onState = "on";
char* soundState = "sound";

// Button states
const int buttonPin = 5;
int buttonState = 0;
int buttonFlag = 1;

// Sound sensor
int soundPin = A0;
int soundValue = 0;
char* soundValState = "on";

// Light sensor
int lightValue = 1;

void setup() {
  // initialize the pushbutton pin as input:
  pinMode(buttonPin, INPUT);

  // Initialize the BUILTIN_LED pin as an output
  pinMode(BUILTIN_LED, OUTPUT);     
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

  // initialize the LCD
  lcd.begin(4,0);  // sda=4, scl=0
  lcd.backlight();
  lcd.clear();
  lcd.print("Chill");
}

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // Switch on the LED if an 1 was received as first character
  if ((char)payload[0] == '1') {
    digitalWrite(LED_BUILTIN, LOW);   // Turn the LED on (Note that LOW is the voltage level
    // but actually the LED is on; this is because
    // it is acive low on the ESP-01)
    lightValue = 1;
  } else {
    digitalWrite(LED_BUILTIN, HIGH);  // Turn the LED off by making the voltage HIGH
    lightValue = 0;
  }

}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266Client")) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish(outTopic, offState);
      // ... and resubscribe
      client.subscribe(inTopic);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
void loop() {

  // Network
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  // Button
  buttonState = digitalRead(buttonPin);
  if (buttonState == HIGH) {
    // not pressed
    // Serial.println("Button: HIGH");
    buttonFlag = 1;
  } else {
    // pressed
    if (buttonFlag == 1) {
      // Serial.println("LOW");
      // Handle state changes
      if (state == onState) {
        state = soundState;
        client.publish(outTopic, offState);
        lcd.clear();
        lcd.print("Sound");
      } else if (state == soundState) {
        lcd.clear();
        // client.publish(outTopic, offState);
        lcd.print("Chill");
        state = offState;
        client.publish(outTopic, state);
      } else if (state == offState){
        state = onState;
        client.publish(outTopic, state);
      }

      // Publish state changes
      Serial.print("Publish message: ");
      Serial.println(state);
      // client.publish(outTopic, state);
    }
    buttonFlag = 0;
  }

  // State
  if (state == onState) {
    lcd.print("~Rave~");
  } else if (state == soundState) {
    if (lightValue) {
    soundValue = analogRead(soundPin);
      if (soundValue >= 1020 && soundValState == "off") {
        soundValState = "on";
        client.publish(outTopic, soundValState);
      } else if (soundValState == "on") {
        soundValState = "off";
        client.publish(outTopic, soundValState);
      }
    }
  }

  delay(50);
}
