#Project 3 App

##The App
The source code for the application can be found [here](app/ssc/src/)

The application is a Web App, which can be run on a webserver like any other webpage on port 80 with the help of some webserver like Apache.

Ionic handles the UI to be adaptive to the device the Web App is ran on. When running the App on a Smartphone it will look like this:

![](project3_media/flatty_mobile.png)

When running the app on a smartmirror, it will adapt its interface to a desktop web browser, also the user can invert the colors, since a smartmirror needs a black background to reflect and work like a normal mirror:

![](project3_media/flatty_smartmirror.png)

The application is realized as a thin client, meaning it only contains UI code and queries the webserver for access to its businesslogic.