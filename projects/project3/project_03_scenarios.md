#Project 3 Scenarios
##Outline
Outline an IoT scenario covering at least 4 different domains, which mostly can be
implemented with the existing hardware.

- Domains can be for example: comfort, entertainment, nutrition, wearable computing, health, arts, security, privacy, lifestyle

- Fully spec the scenario into a playable pitch which will be enacted by all team participants in final presentation

- Realize the scenario in soft-, hardware, document development (including problems), specify
who has done how much in respective portfolios

- Wrap it into a sales pitch + presentation including the play-through with real hardware of the
scenario.

- Practice and record the scenario play-through as a video and link it in the portfolios.

- The project does not need to generate money, a community benefit is enough.

- The scenario has to be approved by the instructor and can be altered during the development process in accordance with the instructor.

- Present the scenario on the final presentation day.


##The Scenario
Sanitary Support Center - SSC

Domains:

- comfort
- privacy
- health
- data analytics

Persons:

- Michael
- Lorenz Graf
- Patrick
- Martin
- Matt

###Preface

Michael, Lorenz and Matt have an apartment together. All three of them visit the FH and share the same course of studies. To better manage the time each one uses in the bathroom and toilet, the SSC intervenes as a management interface. The SSC tells each one of them, when the WC is occupied or someone is using the bathroom. Additionally, the SSC can tell how long after a session on the toilet, it should not be used.

###Story

It is early in the morning, shortly after the alarm clock of Lorenz went off at 8:30 he wakes up and looks at his [phone]. He sees there, that the toilet is currently occupied and decides to sleep for just five more minutes. As always he overslept and is only woken up by his urge to pee. He checks his [phone] and sees that the toilet is no longer occupied but the Smell-O-Meter(tm)'s smellevation is still at critical levels. He sees that it will take two more minutes for things to settle and brushes his teeth first. On the smart mirror he opens the statistics window and sees, that Michael needed really long in the shower last week and thinks about how they will distribute the shower time for this week. With his morning routine in the bathroom finished he now finally got time to settle his business on the toilet. Even before entering the toilet he checks the app if there are enough backup toilet paper rolls ready. He sees that there are none left and gets a few toilet paper reserves from the closet. Immediately after closing the door behind him the system sees that the toilet is now occupied. After finishing his session he chooses 5 minutes of recommended waiting time before the next person enters the stall (other options would be 10 or 15 minutes). He then rushes to his IoT course in the FH and presents his final IoT project concept.
