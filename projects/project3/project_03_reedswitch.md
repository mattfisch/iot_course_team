# Project 3 Reed Switch (done by Michael Rieger)

The toilet door switch was realised with UlnoIoT after noticing that working with the Arduino IDE will produce more work.

The implementation of the toilet switch was a rather easy task. All I had to do was to connect the wemos with our WiFi, initialize the MQTT connection and tell it to listen for a digital signal on d1.

The current status of the switch is then being sent to our Node-RED backend and then sent to our database.

```python
# This is the autostart code for a ulnoiot node.
# Configure your devices, sensors and local interaction here.

# Always start with this to make everything from ulnoiot available.
# Therefore, do not delete the following line.
from ulnoiot import *

wifi("iotempire-CooleBoys", "abcde12356")
mqtt("192.168.12.1", "flatty/toilet")
contact("toilet_switch", d1, "open", "closed")

run(0)
```

(Unfortunately I couldn't find the picture I took of the switch)
