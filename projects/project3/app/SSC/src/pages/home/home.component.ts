import {Component} from '@angular/core';
import {NonosPage} from "../nonos/nonos.component";
import {ShoppinglistPage} from "../shoppinglist/shoppinglist.component";
import {SscPage} from "../ssc/ssc.component";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  tab1Root = NonosPage;
  tab2Root = ShoppinglistPage;
  tab3Root = SscPage;

  constructor() {
  }
}
