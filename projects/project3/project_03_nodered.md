#Project 3 Integrator

##How it works
The Node-Red integrators job is to connect the nodes to the backend and work as broker between all components. He runs a MQTT-Server to receive messages from the nodes containing information about the current toilet status, the number of toilet paper rolls and responses to the shower controls requests for a list of registered users in the flat. When the users are requested, the integrator calls the API of the backend and requests a list of users, which in turn queries the MySQL database, returns the results in JSON format and sends a HTTP-Post response to the Node-Red integrator, which parses the JSON into a string with Pipes "|" as delimiter between users, since they are less overhead than a JSON string, which is the sent back to the showernode using a MQTT_send command with the string as payload.
![](project3_media/node_red.png)

Same process is executed for data beeing pushed to the backend. The data is parsed into JSON and sent to the PHP API using HTTP-POST Messages with the JSON data in the body aiming at the correct API endpoints to save the data into the MySQL Databases storage.

##Problems we had
The firewall of our webserver was very strict, blocking out specific IP addresse when he felt like the IP address was attempting a DDOS attack (in reality we just had a way to hight update rate for our values and the servers firewall tought it was a DDOS attack). This way we locked ourselvs out of our own server several times.
