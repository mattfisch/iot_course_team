#The story

##Scene 1
**Matthias sits on the toilet and introduces himself and the projects outlines to the audience.**

"Oh hey there! I didn't notice you! How are you? Well as you can see I'm a little busy in here, but let me quickly elaborate, what you are going to see in a few seconds. Our little story takes place in a household with three men. All three of them follow nearly the same morning routine. We will see the same story twice, first without the Sanitary Supervision Center installed in the household, and the second time with it being installed. Well, without further interruptions, let me tell you a tale of ancient times, when man did not yet know if the toilet is occupied or not.

##Scene 2
**Lorenz lies in his bed and wakes up because he needs to pee really really bad**

"Damn it! I have to pee really really bad!" **he says as he gets up and moves to the toilet.**
"Oh no! It seems like someone is already occupying the toilet, it would have been so practical to know if it was occupied before I got up from bed!"

**He hears someone knocking on the door - Its the postman who got a letter with the monthly water bill**

"Here I got a letter for you." **Postman shows the water bill to Lorenz** "Oh my god!" **Lorenz says** "If there was just some way to know who of us wasted so much water?" **He says to himself rubbing his chin** "Well seems like we just have to split the bill equally... again" **He says and shrugs with his shoulders**

**Lorenz goes into the bathroom and starts brushing his teeth**

**Meanwhile in another room**
**Martin gets up from bed and immediately has to pee** "Damn, I have to pee" **He says to himself as the moves to the toilet**

**After finishing his business he grabs towards the toilet paper holder** "Damn it! no toilet paper left! **He discovers with dismay** "If there was just some way to tell, how many rolls are in the toilet at any given time..." **he says**

**Lorenz finished brushing his teeth and wants to go to the toilet, but no! its occupied again!**

"Darn it, why can't I just get some time to pee!" **He says as he goes back into his room with his head down**

##Scene 3
**Lorenz lies in his bed and wakes up because he needs to pee really really bad**

"Damn it! I have to pee really really bad!" **he says as he gets up and looks at his phone, opening the Flatty app on his phone.**
"Ah, someone is still on the toilet, good to know, I will sleep five more minutes and go, when its no longer occupied"

**He hears someone knocking on the door - Its the postman who got a letter with the monthly water bill**

"Here I got a letter for you." **Postman shows the water bill to Lorenz** "Oh my god!" **Lorenz says** "Luckily I can see the shower stats on the smart mirror and in the Flatty app! This way I can find out, who wasted so much water" **He says to himself snipping with joy on his face** "Ah! Michael showered really really long last month, he will pay the most!" **He says and puts a smile on his face as he goes to confront Michael**

**He knocks at Michael's door and enters his room immediately. They look at each other and Lorenz says:**
"You take way too long in the shower, dude!"
"Whatever, I'm kind of in the middle of something here"**Mike replies**

**Lorenz takes a look at the app and sees that there are no toilet paper rolls left, he grabs a couple from the shelf before he goes to the toilet**

**Meanwhile in another room**
**Martin gets up from bed and immediately has to pee** "Damn, I have to pee" **He says to himself as he opens the Flatty app** "Seems like someone is still occupying the toilet, I will put on some clothes and brush my teeth before going to the toilet" **He says as he gets up**

**Lorenz finished his business and joins martin, who is currently brushing his teeth.**
**Both nod to each other and are happy that they didn't waste any time**
