# Authentication
### This API is no longer active.
#### URL
	http://ssc.radix-project.com/login.php

#### POST
	string:		name			Name of WG
	string:		secret		Secret of WG

#### Result
	int:			id				-1 if error, id of WG if success

# Getter
## Get Toilet Paper
#### URL
	http://ssc.radix-project.com/getTP.php

#### POST
	string:		secret		Secret of WG
	int:			id				Id of WG of interest

#### Result
	int:			rolls			amount of rolls still available

## Get Toilet Occupation
#### URL
	http://ssc.radix-project.com/getTO.php

#### POST
	string:		secret		Secret of WG
	int:			id				Id of WG of interest

#### Result
	int:			state			0 if empty, 1 if occupied, 2 if on timer

## Get Users
#### URL
	http://ssc.radix-project.com/getUsers.php

#### POST
	string:		secret		Secret of WG
	int:			id				Id of WG of interest

#### Result
	string:			users		"user1|user2|user3"

## Get Shower Statistic
#### URL
	http://ssc.radix-project.com/getShowerStats.php

#### POST
	string:		secret		Secret of WG
	int:			id				Id of WG of interest

#### Result
	string:			users		"TimerPerShower1|TimerPerShower2|TimerPerShower3"

# Setter
## Set Toilet Paper
#### URL
	http://ssc.radix-project.com/setTP.php

#### POST
	string:		secret		Secret of WG
	int:			id				Id of WG of interest
	int:			rolls			Amount of rolls

#### Result
	int:			result		1 if success, -1 if error

## Set Toilet Occupation
#### URL
	http://ssc.radix-project.com/setTO.php

#### POST
	string:		secret		Secret of WG
	int:			id				Id of WG of interest
	int:			state			0 if empty, 1 if occupied, 2 if on timer

#### Result
	int:			result		1 if success, -1 if error

## Increment Shower Time
#### URL
	http://ssc.radix-project.com/setTO.php

#### POST
	string:		secret		Secret of WG
	int:			id				Id of WG of interest
	int:			id				User identifier
	int:			time			Additional shower time in seconds

#### Result
	int:			result		1 if success, -1 if error
