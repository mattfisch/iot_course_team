#Project 3 Backend
The backend was realized using a combination of MySQL and PHP. The method of choice for communication is JSON.

An API documentation document can be found [here](backend/api_documentation.md).

## Structure
The structure of the database confirms the third normal form. To see a more detailed description of the backend structure please refer to [this document](backend/backend_desc.pdf).

## MySQL
To reconstruct the backend the code for cloning can be found in the [SQL file](backend/backend.sql).

## PHP
To interact with the backend PHP files were used. They all follow a similar path:

  1. Authentication (Database)
  2. Set Headers
  3. Connect to database
  4. Decode Input
  5. Authentication (User)
  6. Build SQL query
  7. Execute SQL query
  8. Parse result to JSON
  9. Return result

The resources can be fund [here](backend/).
