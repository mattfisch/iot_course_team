#Project 3 Toilet Paper Counter

##How it works

The toilet paper counter is a simple scale, which counts the weight on it and it sends the data via MQTT to the integrator, the Node-red. On the node-red we divide the whole weight through the weight of 1 toilet paper roll, so we get the number of toilet paper rolls on the scale. The data(weight) from the scale gets counted every second, and only sends it to the node-red integrator if the weight changes.

![](project3_media/ToiletPaperCounter.png)

After the toilet paper got counted, node-red sends the received data via a http request to the backend, where the amount of toilet paper gets stored in our database. And you can see the amount of toilet paper in the application.

![](project3_media/ToiletPaperCounterApp.png)

The only thing we needed for the scale was the scale itself, the i2c which is inside the scale and a Wemos to send it to the Node-red via MQTT. 

![](project3_media/ToiletPaperCounterAufbau.png)


##Problems we had

One big problem we had, was that 2 of our USB-Ports of our Raspberry Pi were broken, so we were wondering why sometimes the MQTT would not send anything to the Node-Red. A very big problem in the future can be if you buy different toilet paper, because we had to hardcode the weight of 1 roll into the node-red, if for example 1 roll weighs 90 grams, and then you buy another package of toilet paper, which weighs 130gram each, you gonna get a problem after like 5 rolls. Another problem is, that you need 2 power outputs, for the Scale and the Wemos itself, so we needed quite a lot of power banks, because we had no power outlet in our bathroom.
