#Project 3 Shower Control

###My Part
My main part on this project was to create the shower component for this project and do the cutting of our video with Adobe Premiere.   
Source code: [Project03/Workspace](../Workspace)   
Here is a link to the final presentation (Video can be found there):  
[link](https://docs.google.com/presentation/d/1xUa65lGY8kK-s7qgtH-Ps1o-JEaQV6T5J1ugAqb8acU/edit)

The purpose of the shower component is to tell who showered for how long in the selected shared flat. This data helps to split the water bill to fair parts.  
I was happy to be responsible for a hardware part as I had not that much experience with Arduino programming until now and could now gather some while working on a IoT-project, which is a great usecase for microcontrollers!
(Sorry on this point for not doing it with ulnoiot...It is a really awesome framework for sure, but due to my lack of Arduino-experience I prefered to do this with Arduino....)  

At first, I started to gather the functional requirements for the component:

- Select your user to measure the time spent in the shower
- Publish the time a user spent in the shower to a seperate topic  
- Get the list of current users in the shared flat by subscribing to a MQTT topic  

Then, I made the first drafts for achieving this requirements:

- A own button for each person in the shared flat => not very practical, as we do not know how many residents a flat will have
- Use three buttons and a I2C - display in order to go through the list of users (up + down) and select a user (third button) => okay, but meh...there are cooler ways to do this
- Use a joystick to go through the list of users (up+down) and select a user (by pressing it) => That's the way to do this

To prevent people (like me) who loose the sense of time while being in the shower from staying in there longer than they want, I decided that the measurement of the time spent in the shower should not be done by a stopwatch.   
Instead, it makes more sensor to allow the user to select the time he/she wants to spend in the shower before an alarm goes off. To see how much time is left even from inside the shower, a LED strip represents the time spent and left as a ratio to the selected time. => All LEDS on => 100% of the time left, 4 LEDS on => 50% of the time left, 0 LEDS on => time is up!

To enable the system I chose the following devices:  

- The I2C display to select the user and the desired shower time
- A WS2812 Led Strip (NeoPixel) to show the user how much time is left
- A passive buzzer to play a little hymn when the time is up  

Additionally, I chose to append one "Guest" user to list of users I receive. This users can also choose the time he/she wants to spend inside the shower, but the recorded time will not be published (therefore the guest can use water "for free").

Finished "Shower - Component" (sorry for provisional wiring):
![alt-text](./shower_component.jpg)

###Joystick implementation:

Joystick library: [link](https://www.sunfounder.com/learn/rfid-kit-v2-0-for-arduino/lesson-27-joystick-ps2-rfid-v2-0-for-arduino.html)

Errors/Mistakes I made:   
Small ones: COM Port not specified, Wrong board, Wemos broken
One bigger: The ESP8266 only has one analog Port! When taking a look at the Joystick we have five wires:  

- One which should be attached to a digital port: The button
- One which represents the horizontal joystick movements -> **should be attached to an anlaog port**
- One which represents the vertical joystick movements -> **should be attached to an anlaog port**
- Power supply
- GND

Possible solution: Attach one of the joystick - wires to a A/D converter  
Better solution: Being able to only read two directions (e.g. left/right) is not a real problem. Actually, it is perfectly fine to only read up/down or respectively left/right movements as we only need the joystick to:

- Go through the list of users
- Select the desired showering time
As not both selections have to be done at the same time and can therefore be two seperate steps, just being able to recognize up/down movements and the button press (digital pin) is enough.  

Measuring the joystick output led to this result:  
![alt-text](./joystick_down.PNG)
```
int yValue = analogRead(yPin);
int btnValue = digitalRead(btPin);

lcd.clear();
lcd.backlight();

if(yValue < 25){
  lcd.print("UP");
} else if (yValue > 1000){
  lcd.print("DOWN");
} else {
  lcd.print("Neutral");
}
if(btnValue==0){
  lcd.print("PRESSED");
}

```
- Value when neutral state about 500
- Value up about 15
- Value down about 1023
- Button Pressed: 0 on digital Port (low-active)

###Buzzer:

I was kind of surprised two find two kind of buzzers in our Sunfounder Sensor Kit: an active buzzer and a passiver buzzer.  
The difference is that while the active buzzer always plays the same tone when activated, the passive one can play a desired tone. To create a little hymn and select a tone, I used the passive buzzer.  
e.g. `tone(buzzPin, 1000);` => Second is frequency of the tone
**Note:** Somehow the passive buzzer made a silet, but noticeable weird noice after being deactived with *notone()*

###LED-Strip:

The LED-Strip should show the remaining percentage of the selected shower-time. For this I used this calculation
```
double percent = (double)((double)timeInShower / (double)selectedTime);
```
I had some problems as I forgot to cast timeInShower (long) and startShowerTimestamp to *double* values and therefore the calculation always returned 0...But this is stupid and I should have known better => long division = long  
Additionally, I used startShowerTimestamp as the divisor...Which is also stupid (selectedTime is right)...Yes, it was already late at night after a long day.  

I used this library for accessing the LED-Strip:  
![alt-text](./neopixel_library.PNG)  

Unfortunately, this library has some bugs. E.g. The documentation states that when setting up the strip I can turn off all leds with this code:  
```
void setup() {
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
}
```
But when using with the LED Strip we had, the first LED continued to be enabled. (Therefore I had to manually turn it off).

###I2C:

I used this library for achieving the MQTT functionality: [alt-text](http://pubsubclient.knolleary.net)   

In the lab, we agreed on using this topics to use for requesting and receiving the user names and sending the time spent in the shower for the various users:
```
static const char TIME_TOPIC[] = "flatty/shower";
static const char USER_TOPIC[] = "flatty/users";
static const char REQUEST_USER_TOPIC[] = "flatty/requestUsers";
```

One of the problems I encoutered in the actual coding part was that at compile-time, I do not know how many users I will receive. The users come it a format like this (string):   
**Lorenz|Matthias|Michael|Patrick|Martin**

I split them with this code (simplified - no exception handling and no space is reserved for a guest user here):
```
void splitStrToArray(){
  int i=0;
  residents[i] = strtok(residentsStr,"|");
  while(residents[i]!=NULL)
  {
     residents[++i] = strtok(NULL,"|");
  }
  residents[i++] = "Guest";

  residentsNumber=i;
}
```
This code splits the string by every occurence of the pipe and adds the string to the residents array. Since strtok always splits the string till the next occurrence. When passing *NULL*, strtok continues to split the last passed string.

The problem now is that the residents array is a global variable and the size has to be known at compile time. Since we do not know how many residents a shared flat has, I could not either always realloc memory or - simpler - definer a big enough array. We limited the number of users per flat to 10, which is certainly enough.

Most of the time I was searching an error in the following code (simplified to the important basics):

```
void setup() {
  //....
  client.setServer(mqtt_server, 1883);
  client.setCallback(mqtt_message_received);
  //....
}

void setup_wifi() {
  //Connect to WiFi
  //....
}

void mqtt_message_received(char* topic, byte* payload, unsigned int length) {
  //...Note: this code only shows what happens when the USERS_RECEIVED_TOPIC is received!

  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  //....Process the received data

  splitStrToArray();
  usersSet = true;

}

void reconnect() {
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ShowerClient")) {
      client.subscribe(USER_TOPIC);
      client.publish(REQUEST_USER_TOPIC, "sendUsersPls");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  //....
}
void loop() {

  if (!client.connected()) {
    reconnect(); //Connect to broker
  }
  client.loop();
  if(usersSet){
    if(!countDownRunning){
      readJoystick();
    } else if(millis() - startShowerTimestamp > selectedTime*1000) { //TODO: SEC TO MIN HERE
      timeIsUp();
    } else {
      showRemainingTimeWithStrip();
    }
  }
}
```


As you can see, I set the *mqtt_message_received()* as the callback of the MQTT client. As soon as a valid connectoin to the MQTT server is established in the *reconnect()* function, the message "sendUsersPls" gets published. The logic Patrick implemented in NodeRed now answers with a string containing the name of all users of the desired flat, seperated with a pipe. According to the console, this was actually the case, and the broker also got by message, which means that I was really successfully connected to the right server. Nevertheless, the callback function never got called - even when I removed all the logic which decided what to do with the various topics (therefore: It was not called, no matter what topic I subscribed to and to which topic a message was published). I also thought that maybe I publish the request message too soon after establishing the connection and therefore published it in a loop until the callback is called. The result was that our MQTT broker freezed and I still my callback funtion was not called. Also, this could not be the error, as we saw my request on the Raspberry Pi even if I sent it one time (after the successful connection - just as in the code above).  
When testing with the example mqtt project from the library I used ([this](http://pubsubclient.knolleary.net)), I was able to receive the messages just as I wanted to. Therefore the error had to be in my code. I tried moving the callback function to different places as well as subscribing to the topics on multiple lines (and severaly functions), but did not find any mistake.    

**Solution:** After nearly two hours I realized that my *mqtt_message_received()* function is called *callback* in the example code. I did not believe that this would be my "mistake", but after I named the function the same way as they do in the example, I got the messages in my project... YEEEJJJ....Well....

## Presentation

For the presentation, I searched for a good template together with Michael and thought of how we can structure our presentation into meaningful sequential parts. Afterwards, I wrote the "Customer Reviews" we used in the presentation.   
Then, I went to cutting our presentation video with Adobe Premiere and added the audio tracks Michael just recorded. Matthias assisted me here, as he knew which order the videos should be and which of our records is the one they decided to use when filming.  
Here is a link to the final presentation (Video can be found there):  
[presentation](https://docs.google.com/presentation/d/1xUa65lGY8kK-s7qgtH-Ps1o-JEaQV6T5J1ugAqb8acU/edit)
