# user.py is the autostart code for a ulnoiot node.
# Configure your devices, sensors and local interaction here.

# Always start with this to make everything from ulnoiot available.
# Therefore, do not delete the following line.
from ulnoiot import *

mqtt("192.168.12.1","raveintegrator/lightstate")
out("onboard",onboardled,"off","on")
rgb_multi("ledstrip1",d4,8)
run(0)
