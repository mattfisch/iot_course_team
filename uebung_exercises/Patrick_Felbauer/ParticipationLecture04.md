#Lecture 4 (11.10.17)

###What will be issues scaling(system size, number of systems, management)?
The more devices, the more connections you have to make, which propably will drain more battery and data over time.

If you want to update your system, you have to update every single system and not just one.

System size equlas more systems, which is pricier

Predictive maintainance

###How can we do testing?
Unit Tests 

Try and Error 

Testing Frameworks

Simulate a lot of Subscribers to stress test the system

###What role will play
*Simulator - Pre build phase, where you prebuild the system you want to have, to figure out possible problems. Simulators can be used to test the system, without spending too much money on real devices.
 
*MQTT - Broker, which can be subscribed to and which will publish the information to the subscribed components. Single point of failue when not implementing more brokers.

*Stories - Describe the use cases. Help a lot in communication. With stories we can describe the problem easily. Non-technical level of abstraction. Used for finding corner - cases / tests.

## IoT Frameworks: How can we develop and manage Software faster for IoT

###Title: Node-Red
Node-Red is a programming tool for wiring together hardware devices, APIs and online services in new and interesting ways.

It provides a browser-based editor that makes it easy to wire together flows using the wide range of nodes in the palette that can be deployed to its runtime in a single-click.

It is a software tool developed by IBM for wiring together hardware devices, APIs and online services as part of the Internet of Things.
The runtime is built on Node.js and the flows created are stored using JSON

###Publicly availiable/open source/reference implementation
It is publicly availiable and open source on [GitHub](https://github.com/node-red/node-red):

###How good and plentiful is documentation? How easy to understand?
There is a whole [documentation](https://nodered.org/docs/) about:

* Getting Started
* User Guide
* Cookbook
* Creating Nodes
* Developing the core
* API Reference

The documentation is quite Ok, there is a lot of "In-Line" Documentation, but not explicitly what the lines are doing, just for the methods

###WHich devices/operating systems are supported(also as nodes)?
* Edimax SP-2101W
* Orvibo WiFi Smart Socket S20
* Dilisens Smart Outlet
* Lucky Clover Smart Outlet
* Akface™ Smart Wifi Plug
* Atmel Wifi and Bluetooth ATSmartplug
