#Lecture 7 (23.10.17)


- What is broken(with examples)
- How can it be fixed/counter measures?
- Newsworthy failures(successes)


##Research papers/articles on the Internet of Broken Things(Pro's)
Sources:
- [IoT-internet-of-things-good-bad-ugly](http://anandmanisankar.com/posts/IoT-internet-of-things-good-bad-ugly/)

- [Security of the Internet of Things: perspectives and challenges](https://link.springer.com/article/10.1007/s11276-014-0761-7)

#####IoT has the potential to touch every domain, and nearly every aspect of human life. These numbers indicate the massive scale of impact IoT is expected to have:
Imagine everything was linked…

-50 billion devices connected to the Internet worldwide by 2020

-3.5 connected devices per person by 2015 and almost 7 by 2020

-8 billion mobile broadband access points by 2019

-4.5 million IoT jobs by 2020

#####At the core of IoT is the evolving relationship between humans and machines (or things, in general) - a relationship that is being redefined through technology, narrowing the divide between the two.

-Augmenting humans with technology: augmented reality, bio-acoustic sensing, wearables, gesture control, 3D bio-printing…

-Machines replacing humans: Driverless cars, robots, virtual assistants…

-Humans and machines working together: NLP, HCI, virtual assistants…

-Machines better understanding humans and the environment: Geo-location, biometrics, 3D scanners, mobile health monitoring…

-Humans better understanding the machines: IoT, M2M, Mesh networks…

-Humans and machines becoming smarter: Neurobusiness, predictive and prescriptive analytics, complex event processing…
