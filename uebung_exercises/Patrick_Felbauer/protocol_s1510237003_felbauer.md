#IOT protocol
###Setup Raspberry Pi and the SD card:


Because I wasn´t here for the first two lessons I started with the protocol for Lab1 and Lab2 on my own, and joined group Fischbacher and Graf in the LAB3.
I started with setting up the Raspberry Pi and flashed the SD card.


[protocol](../shared/01_io_miniOS_setup.md)

###Remotely toggle the LED on the Arduino:

Then I wrote the LEDServer.ino file and flashed it on the Arduino

[protocol](../shared/03_remote_led_ping.md)

Afterwards I created the HttpClient.ino and HttpClientLCD.ino files, which send a push notification to my mobile phone via IFTTT and flashed them onto the Arduino.

[protocol](../shared/02_iftt_remote.md)

At this time I joined the team Fischbacher and Graf, and we wrote the user stories together.

For the user stories see: [user stories](../shared/04_user_stories.md)

##11.10.2017

We used Node-Red as an integrator and created a few simple programms, like a hello world program.
We also used the Mongoose web based IDE, which supports C and JavaScript code and runs it on the wemos D1 mini.
This Lab was also the start of our project1.

[protocol](../shared/06_NodeRed_MongooseOS.md)

##16.10.2017

We started to install ulnoiot framework on our raspberry pi. After we installed the framework we rebuilt our project 1 with ulnoiot. We had quite a lot of problems with it, which we documented in our protocol.

[protocol](../shared/07_Rebuild_Project1_With_UlnoIoT.md)

##17.10.2017 ... 23.10.2017
During Lab five and six, we created the sensoring node, while matthias created the acousic and rain sensors, i created the light distance sensor. While Matthias and I have done the sensors, Lorenz assembled and connected the scale. Everything we have done we documented in our protocol.

[protocol](../shared/08_sensoring_fluids.md)
