#Lecture 8 (24.10.17)

## LORA
Low Power Wide Area Network = LPWAN
### What is the relation bandwidth/range/power?
The smaller the bandwidth the lower the power needed.
Wi-Fi has a low range but a high bandwidth.
Faster Standard needs more Power.
(modern)Mobile Internet has medium Range and high Bandwidth.
Bluetooth runs well on small devices, but has a low range.
Lora has long range but really low bandwidth.
LoRa is a new transmission standard between distributed devices and distributed gateways.


### What is the Link budget?
Something you have at the beginning and spend over time, if it is spent you can't spend anymore.
It is between the Sender and the receiver, and is calculated in decibel.
Distance, cables, trees,... lower the link budget
If the link budget is up, the receiver only creates noise.
154 dB = LoRaWAN
10km of cables = about 1000km of air.
Sometimes we have to work with reflections, because there is no sight between receiver and sender.
868 MHz in Europe.
The allowed power is only 25milli Watt(Europe).
Data rate = 250 bps- some kbps

### What is the community approach?
There is a Lora Alliance, which has a lot of companies supporting it.
Many devices connect to some Gateways which connect to a Network Server.
You just have to connect your device to the network.
"The Things network" is the community approach. They need many Gateways to work, whom have to be maintained by private Persons.
8parallel channels = 8 devices.
The bandwidth is reduced, because  you can't use it for 100% of the time, you only can use it about for 1% of the time.


###What are benefits with LORA?
LoRa prefers the connection from the sensor to the Gateway than the other way round.
It has extremely low channel capacity, a very low power consumption and therefore a very high link budget.
it is ideal for low power sensors distributed everywhere, also far from a gateway.

###What are problems with Lora?
They are not so cheep, because they only belong to 1 company.
LoRa can only connect in one direction at a time.

### What is LoRa + examples
A link budget is accounting of all of the gains and losses from the transmitter, through the medium (free space, cable, waveguide, fiber, etc.) to the receiver in a telecommunication system. It accounts for the attenuation of the transmitted signal due to propagation, as well as the antenna gains, feedline and miscellaneous losses. Randomly varying channel gains such as fading are taken into account by adding some margin depending on the anticipated severity of its effects. The amount of margin required can be reduced by the use of mitigating techniques such as antenna diversity or frequency hopping.

#####Examples
Earth–Moon–Earth communications
Link budgets are important in Earth–Moon–Earth communications. As the albedo of the Moon is very low (maximally 12% but usually closer to 7%), and the path loss over the 770,000 kilometre return distance is extreme (around 250 to 310 dB depending on VHF-UHF band used, modulation format and Doppler shift effects), high power (more than 100 watts) and high-gain antennas (more than 20 dB) must be used.
In practice, this limits the use of this technique to the spectrum at VHF and above.
The Moon must be above the horizon in order for EME communications to be possible.

Voyager Program
The Voyager Program spacecraft have the highest known path loss (-308 dB as of 2002) and lowest link budgets of any telecommunications circuit. The Deep Space Network has been able to maintain the link at a higher than expected bitrate through a series of improvements, such as increasing the antenna size from 64m to 70m for a 1.2 dB gain, and upgrading to low noise electronics for a 0.5 dB gain in 2000/2001. During the Neptune flyby, in addition to the 70-m antenna, two 34-m antennas and twenty-seven 25-m antennas were used to increase the gain by 5.6 dB, providing additional link margin to be used for a 4x increase in bitrate.

### 2 calculations for LoRa and Wi-Fi
LoRa:
1. -128.9dBm for 27km
2. -153.3dBm for 456km

WiFi:
-122.3dBm for 27km
-152.5dBm for 456km

### LoRa in Austria/linz
LoRaTM has been specifically designed to meet the needs of the Internet of Things. LoRa demonstrates its advantages especially in the live transmission of small amounts of data. Kapsch, Microtronics and ORS comm are building an Austria-wide LoRaWANTM. The LoRaWAN is based on the open standard of the LoRa AllicanceTM.
A LoRaWAN can be set up independently of a network provider, for example to implement applications locally for your company premises. However, if you want to develop an application across several locations and possibly with mobile sites across Austria, it is useful to use LoRaWAN from Kapsch, Microtronics and ORS comm.

### TTN
The Things Network
Building a fully distributed Internet of Things data Innfrastrucutre.
The Things Network is building a network for the Internet of Things by creating abundant data connectivity, so applications and businesses can flourish.

The technology we use is called LoRaWAN and it allows for things to talk to the internet without 3G or WiFi. So no WiFi codes and no mobile subscriptions.

It features low battery usage, long range and low bandwidth. Perfect for the internet of things.

The Things Network also has a Kickstarter page, where you can support them.
There is very few coverage in our area.
