#Lecture 3 (11.10.17)

###Text format
Data Format that takes very few memory, just strings
it has to be seperated manually, by an indicator, for example with a "," or a "/n"

It has no Schema, because the indicator which seperates the arguments has to be entered manually
Human readability depends on argument, if you just enter numbers without any statement in front of it it is readable, but the user doesn't know what the numbers mean.

It is very fast to write, because you just have to add argmunets.

The user friendlyness depends on the argument, names are quite clear but some other parts might be strange.
It is very hard to translate the language to others, because it has to be in that format to be read.


###Thread Group
Thread Group wanted to build a tchnology that uses and combines the best of what´s out there in IoT and create a networking protocol that helps the IoT realize its potential for years to come.

It is simple for consumers to use, always secure, power-efficient, open protocol that caries IPv6 natively. 

The Thread stack is an open standard for reliable, cost-effective, low-power and wireless D2D (device-to-device) communication. It is designed specifically for Connected Home applications where IP-based networking is desired and a variety of application layers can be used on the stack.

Notable Members: 
ARM, Dell, Microsoft, LG, Amazon, Philips, Samsung, Silicon Labs, Siemens, Verizon

Founders:
President: Grant Erickson, Principal Software Engineer, Nest Labs
He is a principal engineer at Nest, where he oversees the technical development of software designed to support Bluetooth Low Energy, Thread, WiFi, and Nest Weave.
In his role, he was also an early contributor to the formation of the Thread Group and Thread networking protocol. He is currently a member of the Thread Group Board of Directors.

Vice President of Technology: Skip Ashton, Vice President, Software Silicon Labs
Skip is the Vice President of Software at Silicon Labs. He is responsible for managing the company's software and quality assurance teams, focusing on 8-bit and 32-bit MCU driver software and libraries, Ember ZigBee software and software for short-range sub-GHz wireless IC products. Previously was the Vice President of Engineering and Technology at Ember. He also has been involved with ZigBee technology.

###MQTT - (MQ Telemetry Transport or Message Queue Telemetry Transport)
publish-subscribe-based "lightweight" messaging protocol for use on top of the TCP/IP protocol. The publish-subscribe messaging pattern requires a message broker. The broker is responsible for distributing messages to interested clients based on the topic of a message.

Method:
Connect: Waits for a connection to be established with the server.

Disconnect: Waits for the MQTT client to finish any work it must do, and for the TCP/IP session to disconnect.

Subscribe: Waits for completion of the Subscribe or UnSubscribe method.

UnSubscribe: Requests the server unsubscribe the client from one or more topics.

Publish: Returns immediately to the application thread after passing the request to the MQTT client.

Examples:

* Facebook Messenger. Facebook has used aspects of MQTT in Facebook Messenger for online chat

* Amazon Web Services announced Amazon IoT based on MQTT in 2015

* Microsoft Azure IoT Hub uses MQTT as its main protocol for telemetry messages

![](MQTT.png)