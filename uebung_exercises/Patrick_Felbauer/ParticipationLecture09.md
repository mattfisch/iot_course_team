#Lecture 9 (25.10.17)

##Predictive Maintainance

### What is predictive maintenance?
Predictive maintenance techniques are designed to help determine the condition of in-service equipment in order to predict when maintenance should be performed. This approach promises cost savings over routine or time-based preventive maintenance, because tasks are performed only when warranted.

### What problem does it solve?
The main promise of predictive maintenance is to allow convenient scheduling of corrective maintenance, and to prevent unexpected equipment failures. By knowing which equipment needs maintenance, maintenance work can be better planned and what would have been "unplanned stops" are transformed to shorter and fewer "planned stops", thus increasing plant availability. Other potential advantages include increased equipment lifetime, increased plant safety, fewer accidents with negative impact on environment, and optimized spare parts handling.

### How is it related to the IoT
Reduce maintenance cost, increase asset availability, improve customer satisfaction, generate new service revenue and change to a usage-based business model with the Internet of Things (IoT).
IoT for Predictive Maintenance Program: The next wave of business transformation with Industrial Analytics & Machine Learning
Extending Operational services into IoT Solution Managed Services: Real Time Condition Monitoring and Predictive Maintenance
Building the right ‘Failure Model’

### Give 3 examples of predictive maintenance in the IoT
Railway System:
A good example of this in action comes in the rail industry, where a number of fascinating use cases have emerged in the past year.
For instance, Finnish company Sharper Shape have been using drones to map utility networks.  They use machine learning to identify trees that are at risk of falling onto power lines.

Software AG:
Software AG’s solution for predictive maintenance leverages the Internet of Things (IoT) by continuously analyzing real-time equipment sensor data via machine monitoring to understand when maintenance will be required. Technician locations are coupled with replacement/repair equipment available and job completion time to identify the best technician available to perform the needed service during a scheduled downtime.

SAP:
The German software giant SAP has been showing how it enables predictive maintenance for several years now and has thus established itself as the most searched for company in relation to predictive maintenance. Notably, it is one of three German firms in the top 10. SAP has implemented its solution “Predictive Maintenance and Service” for customers such as Kaeser Kompressoren or Siemens and the solution is now part of its newly unveiled SAP Leonardo IoT Portfolio.

### Which role does data analytics play?
Rare events prediction in complex technical systems has been very interesting and critical issue for many industrial and commercial fields due to huge increase of sensors and rapid growth of Internet of Things (IoT). To detect anomalies and foresee machine failure during normal operation, various types of Predictive Maintenance (PdM) techniques have been studied, as shown in the following table.

Enable Domain Experts to do Data Science

## Heartbeat Monitoring of Network Nodes

### How does a heartbeat monitor in a network work?
In computer clusters, heartbeat network is a private network which is shared only by the cluster nodes, and is not accessible from outside the cluster. It is used by cluster nodes in order to monitor each node's status and communicate with each other.
A heartbeat is a type of a communication packet that is sent between nodes. Heartbeats are used to monitor the health of the nodes, networks and network interfaces, and to prevent cluster partitioning.

### What can it be used for?
It can be used to check if a sensor is dead or not and to discover devices with specific services, connect to device, discover services, discover characteristics for a service, how to read value for given characteristic, set notification for the characteristic, etc...
It can be checked by a simple ping.

### What can be secured in an IoT system with this type of monitoring?
It can be used to check if a sensor is dead or not and tell the architecture manager to replace that sensor(s).

###What feature in ulnoiot already supports heartbeats?
command devices, checks which devices are already deployed
