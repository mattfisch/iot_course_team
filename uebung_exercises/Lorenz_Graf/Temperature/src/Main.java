import org.fusesource.mqtt.client.BlockingConnection;
import org.fusesource.mqtt.client.MQTT;
import org.fusesource.mqtt.client.QoS;

import java.net.URISyntaxException;
import java.util.Random;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

public class Main {
    static int interval;
    static Timer timer;

    static final int[] temperatures = {18, 20, 22, 24, 26};
    static final Random random = new Random();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Input seconds => : ");
        String secs = sc.nextLine();
        int delay = 1000;
        int period = 1000;
        timer = new Timer();
        interval = Integer.parseInt(secs);
        System.out.println(secs);
        timer.scheduleAtFixedRate(new TimerTask() {

            public void run() {
                String currentTemperature = temperatures[random.nextInt(5)] + "";

                MQTT mqtt = new MQTT();
                try {
                    mqtt.setHost("192.168.12.1", 1883);
                } catch (
                        URISyntaxException _e) {
                    _e.printStackTrace();
                }

                BlockingConnection connection = mqtt.blockingConnection();
                try {
                    connection.connect();
                } catch (
                        Exception _e) {
                    _e.printStackTrace();
                }

                try {
                    connection.publish("temperature", currentTemperature.getBytes(), QoS.AT_LEAST_ONCE, false);
                } catch (
                        Exception _e) {
                    _e.printStackTrace();
                }

                System.out.println(setInterval() + " Current Temperature: " + currentTemperature);

            }
        }, delay, period);
    }

    private static final int setInterval() {
        if (interval == 1)
            timer.cancel();
        return --interval;
    }
}
