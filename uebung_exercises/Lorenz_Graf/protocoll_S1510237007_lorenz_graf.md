#IOT protocol
This document only contains information about the exercise participation. For the lecture participation proofs please refer to [this repository](https://lorenz-graf@bitbucket.org/lorenz-graf/iot-lecture.git).
##Lab 1 - 02.10.2017
###Setup of the Raspberry Pi and the miniOS on a SD card:

[protocol](../shared/01_io_miniOS_setup.md)

My team colleague and I setup the Raspberry Pi together and flashed some SD cards for our colleagues.

###Remotely toggle the LED on the Arduino:

[protocol](../shared/03_remote_led_ping.md)

Using Pair programming with my team colleague we wrote the LedServer.ino file and flashed it onto the Arduino.

##Lab 2 - 03.10.2017

[protocol](../shared/02_iftt_remote.md)

Each of us created an IFTTT webhook. Using pair programming we coded up the HttpClient.ino and HttpClientLCD.ino files (Which both send a notification to each of our phones when the button attached to the Arduino is pressed) and flashed them onto the Arduino.

##Lab 3 - 11.10.2017

We learned how to use an integrator as controlling part of an IoT environment. We tried out Node-Red as integrator and created a simple hello world program. We also tested out another IDE than Arduino Studio. Mongoose is a web based IDE, which supports C and JavaScript code to be run on the wemos d1. We started our work on project1_1.

[protocol](../shared/06_NodeRed_MongooseOS.md)

##Lab 4 - 16.10.2017

While my colleagues worked on implementing the project 1 in ulnoiot i worked on figuring out the functionality of multiple sensors.

[protocol](../shared/07_Rebuild_Project1_With_UlnoIoT.md)

##Lab 5,6 - 17.10.2017

During lab five and six I build an electric scale and calibrated it. Afterwards I implemented the I²C communication protocol between the scale and the ulnoiot node that now sends MQTT data updates.

[protocol](../shared/08_sensoring_fuilds.md)

##Lab 7 - 24.10.2017
At lab 7 we created a LORAN sender and receiver, the protocol for this can be found here:

[protocol](../shared/09_The_Things_Network.md)

We followed the instructions of the links in the exercises and attached the shield, with which LORAN signals are received from LORAN senders and sent to the TTN page, where we created an account and registered the gateway.

##Lab 8 - 25.10.2017
During lab 8 we started with the project three and presented our results from project two.

Results from project two: [protocol](../shared/08_sensoring_fluids.md)

The documentation of project three can be found here: [protocol](../../projects/project3/project_03_scenarios.md)

We came up with the idea and wrote down a user story for the project three. Additionally the team size changed from three to five, our two colleagues Michael and Martin joined us for the last project.

##Lab 9 - 27.10.2017
In lab 9 we created the mobile application for the project three and started with the toilet paper weighting. I created and hosted the backend, which stores the information about toilet paper and the shower statistics on his webserver.

- The documentation for the backend I made can be found here:
	* sourcecode:
	* documentation: [backend](../../projects/project3/project_03_backend.md)
	* API documentation: [api documentation](../../projects/project3/api_documentation.md)
- The protocol of the mobile application made by Michael, Matthias and me can be found here:
	* documentation: [app](../../projects/project3/project_03_ionic_app.md)
	* source code: [ionic app](../../projects/project3/app/SSC)
- The protocol of the toiletpaper node Patrick made can be found here:
	* documetation: [toilet paper counter](../../projects/project3/project_03_toiletpaper_counter.md)
	* source code: [toilet paper counter](../../projects/project3/ToiletPaperCounter/ToiletPaperCounter.ino)
- The protocol of the showerstats node Martin can be found here:
	* documetation: [shower control](../../projects/project3/project_03_showerctrl.md)
	* source code: [shower control](../../projects/project3/ShowerCtrl/ShowerCtrl.ino)
- The protocol of the toilet-in-use node Michael made can be found here:
	* documetation: [reed switch](../../projects/project3/project_03_reedswitch.md)
	* source code: [reed switch](../../projects/project3/reed_switch)
- The protocol of the Node-Red integrator made by Patrick and me can be found here:
	* documetation: [integrator](../../projects/project3/project_03_nodered.md)

##Lab 10 - 30.10.2017
Continuing our work from lab 9.

##Lab 11 - 31.10.2017
Made the final presentations, including protocolls and the pitch, storyboard and [plot](../../projects/project3/project_03_presentation_plot.md) for the video. The video can be found on youtube: [Flatty ssc](https://www.youtube.com/watch?v=GDgvd_f9TS8)
