#Protocol - exercise lesson 1 -  2.10.2017Setup Arduino IDE MAC:https://www.arduino.cc/en/Main/DonateOr use the web editor:https://create.arduino.cc/editor**Setup bootable SD cards for the raspberry pi:**

* Insert the SD card into your pc* Open the ulnoiot.conf file
* Change the ULNOIOT_AP_NAME value to your team’s name* Change the ULNOIOT_APP_ADDID to “no”* Change the ULNOIOT_AP_PASSWORD![](media/default_values.jpg)* Put the SD card into your raspberry pi
* Boot it up
* Connect to your previously created WLAN (the raspberry pi works as router)
* ssh onto the raspberry pi using the default credentials: ssh pi@ulnoiotgw
* password is: ulnoiot
* move into the directory: cd src/rpi-clone
* run the command sudo ./rpi-clone and follow the instructions**Connect to raspberry pi using VNC**
* Enter the raspberry pi config using sudo raspi-config
* Reboot the raspberry pi
* Run the command vncserver
* Connect to the raspberry pi**Make it blink**
* Open the Arduino ido
* Go to tools>board>WeMos D1 R2 & mini
* Got to tools>ports>usb
* Go to file>example>ESP8266>blink
* Build the example**It might be necessary to change permissions:*** sudo chmod -R 755 /home/pi/apps/arduino