import org.fusesource.mqtt.client.*;

import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;

public class MQTTReceiver {

    private BlockingConnection mBlockingConnection;
    private List<Topic> mTopics;

    public interface OnDataReceived {
        void dataReceived(String _topic, String _data);
    }

    private OnDataReceived mOnDataReceivedCallback;

    public MQTTReceiver(String _ip, int _port) throws Exception {
        mTopics = new LinkedList<>();
        MQTT mqtt = new MQTT();
        mqtt.setHost(_ip, _port);
        mBlockingConnection = mqtt.blockingConnection();
        mBlockingConnection.connect();
    }

    private Runnable receiveMsgs = () -> {
        while (true) {
            Message message = null;
            try {
                message = mBlockingConnection.receive();
            } catch (Exception _e) {
                _e.printStackTrace();
            }

            if (mOnDataReceivedCallback != null) {
                mOnDataReceivedCallback.dataReceived(message.getTopic(), new String(message.getPayload()));
            }

            message.ack();
        }
    };

    public void addTopic(String _topic) {
        mTopics.add(new Topic(_topic, QoS.AT_LEAST_ONCE));
        try {
            mBlockingConnection.subscribe(mTopics.toArray(new Topic[mTopics.size()]));
        } catch (Exception _e) {
            _e.printStackTrace();
        }
        Thread t = new Thread(receiveMsgs);
        t.run();
    }

    public void setOnDataReceivedCallback(OnDataReceived _onDataReceivedCallback) {
        mOnDataReceivedCallback = _onDataReceivedCallback;
    }
}
