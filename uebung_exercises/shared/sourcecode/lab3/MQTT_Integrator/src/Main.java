import java.io.IOException;

public class Main {

    public static final String IP = "192.168.12.1";
    public static final int PORT = 1883;

    public static void main(String[] args) {

        System.out.println("Integrator relay started!");
        MQTTReceiver receiver = null;
        try {
            receiver = new MQTTReceiver(IP, PORT);
        } catch (Exception _e) {
            _e.printStackTrace();
        }

        System.out.println("Starting listening for topic \"temperature\"");

        receiver.setOnDataReceivedCallback((_topic, _data) -> {
            System.out.println(_topic);
            if (_topic.equals("temperature")) {
                System.out.println("Received new temperature value: " + _data + " °C");
                if (Integer.valueOf(_data) > 22) {
                    System.out.println("Temperature too high! Turning on AC");
                    new MQTTSender(IP, PORT).sendMsg("state", "on");
                } else {
                    System.out.println("Temperature too low! Turning off AC");
                    new MQTTSender(IP, PORT).sendMsg("state", "off");
                }
            }
        });

        receiver.addTopic("temperature");



        try {
            System.in.read();
        } catch (IOException _e) {
            _e.printStackTrace();
        }
        System.out.println("cleaing up...");
    }
}
