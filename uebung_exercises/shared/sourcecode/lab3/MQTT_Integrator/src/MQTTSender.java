import org.fusesource.mqtt.client.BlockingConnection;
import org.fusesource.mqtt.client.MQTT;
import org.fusesource.mqtt.client.QoS;

import java.net.URISyntaxException;

public class MQTTSender {

    private BlockingConnection mBlockingConnection;

    public MQTTSender(String _ip, int _port) {
        MQTT mqtt = new MQTT();
        try {
            mqtt.setHost(_ip, _port);
        } catch (URISyntaxException _e) {
            _e.printStackTrace();
        }
        mBlockingConnection = mqtt.blockingConnection();
        try {
            mBlockingConnection.connect();
        } catch (Exception _e) {
            _e.printStackTrace();
        }
    }

    public void sendMsg(String _topic, String _msg) {
        try {
            mBlockingConnection.publish(_topic, _msg.getBytes(), QoS.AT_LEAST_ONCE, false);
        } catch (Exception _e) {
            _e.printStackTrace();
        }
    }
}
