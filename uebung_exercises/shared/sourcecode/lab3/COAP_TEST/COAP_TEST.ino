/*
ESP-COAP Server
*/

#include <ESP8266WiFi.h>
#include <coap_server.h>

coapServer coap;

//WiFi connection info
const char* ssid = "iotempire-CooleBoys";
const char* password = "abcde12356";

// LED STATE
bool LEDSTATE;

void callback_test(coapPacket *packet, IPAddress ip, int port,int obs) {
  Serial.println("Test called");

  // send response
  char p[packet->payloadlen + 1];
  memcpy(p, packet->payload, packet->payloadlen);
  p[packet->payloadlen] = NULL;

  String message(p);

  if (message.equals("0"))
    LEDSTATE = false;
  else if (message.equals("1"))
    LEDSTATE = true;

  if (LEDSTATE) {
    digitalWrite(LED_BUILTIN, LOW) ;
    if(obs==1)
     coap.sendResponse("1");
     else
    coap.sendResponse(ip, port, "1");
    
    //coap.sendResponse("1");
  } else {
    digitalWrite(LED_BUILTIN, HIGH) ;
    if (obs==1)
    coap.sendResponse("0");
    else
    coap.sendResponse(ip, port, "0");
    //coap.sendResponse("0");
  }
}


void setup() {
  yield();
  //serial begin
  Serial.begin(115200);

  WiFi.begin(ssid, password);
  Serial.println(" ");

  // Connect to WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    //delay(500);
    yield();
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  // Print the IP address
  Serial.println(WiFi.localIP());

  // LED State
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  LEDSTATE = true;

  // add server url endpoints.
  // can add multiple endpoint urls.

  coap.server(callback_test, "test");

  // start coap server/client
  coap.start();
  // coap.start(5683);
}

void loop() {
  coap.loop();
  delay(1000);
}
