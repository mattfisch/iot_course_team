#COAP
##Setup
Install coap server from this [source](https://github.com/automote/ESP-CoAP).
Load the sample server and run it using your endpoint.

Whenever a POST with 1 is sent, the onboard led is turned on, whenever 0 is sent, the onboard led is turned off.

###COAP Message to turn LED on
![](media/COAP_ON.png)

###COAP Message to turn LED of
![](media/COAP_Off.png)

#MQTT Simulator
##Temperature sensor
The temperature sensor was simulated in a Java application. It utilized an Array that contains the values 18, 20, 22, 24 and 26 degree Celsius. Every second one of these temperatures is chosen by a random index selection. This value is then sent via MQTT.

##Switch

##Integrator
The integrator polls the MQTT server for the topic "temperature" when a new temperature value is posted by the temperature sensor, the integrator tests it, if its above or below 22 °C he posts a message to the mqtt server for the topic "state" with either "on" or "off".

Main logic: [here](sourcecode/lab3/MQTT_Integrator/src/Main.java)

Sending data to the MQTT server [like this](sourcecode/lab3/MQTT_Integrator/src/MQTTSender.java).

Receiving data from the MQTT server [like this](sourcecode/lab3/MQTT_Integrator/src/MQTTReceiver.java).
