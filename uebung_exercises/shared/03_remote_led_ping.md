#LED SERVER
##Hardware
For this exercice we used the Raspberry Pi and the Arduino without any additional sensor/actor attached.

##Software
Nothing special, except the LedServer.ino file, which is flashed onto the Arduino using the raspberry pi as master and the Wireless Local Network using the Raspberry Pi as router.

##Combine it all
The source code used for this can be found [here](sourcecode/lab2/LedServer/LedServer.ino).

##Usage
Read the IP address of the Arduino, which is printed onto its console while booting the server and connect to it using your webbrowser with port 80.
The browser will return a plaintext response and the led will start/stop blinking every one second.