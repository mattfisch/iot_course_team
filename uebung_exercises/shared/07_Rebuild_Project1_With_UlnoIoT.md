#Rebuild project 1 with UlnoIoT

##Install UlnoIoT on the Raspberry Pi

- Git pull [UlnoIoT](https://github.com/ulno/ulnoiot)

- execute fix_bin

- execute ulnoiot upgrade

- copy folder lib/system_templates to project directory and renamed it to "raverlno"

- renamed the node to ravecontrol

- After that we configured the system.conf where we set our SSID and Password of our network.

- Then we flashed the device by executing initialize

- After that we accessed the command prompt by executing console_serial

- Finally we setup our wifi with wifi("SSID", "Password")

Then we started to play around a bit, with using the help command to find out how to run several comamnds for example help("button").

##Rebuild project 1

First of all we pulled our Git Repository to our Raspberry Pi, where we had our Python Project.

Create a new node-red flow using the web interface where the input is the button which is connected to the Arduino.
![](media/node_red_integrator.png)

The function buttonToggledDistributor which we implemented lets us use the button as a toggle and not just as: Button_pressed = on and button_released = off, but like button_pressed = on and next time button_pressed = off:

![](media/buttonToggleDistributor.png)

The output of the toggle looks like this:

![](media/toggle_on_off.png)

If the button press toggles to the "onState", the onboard Led turns on and also the led Strip turns on.
And we go to the next function where we activate the Rave Mode. First check if the button toggles the "onState", if it does, we set "isLooping" to true, then we choose the color for the LED, where we predefined 4 colors. If the "onSate" is activated, we start a loop, which randomly(between 200 and 600ms) recalls the raveModeColorBrightnessDistributor, to make it look like a rave and change between those 4 colors.

![](media/raveModeColorBrightnessDistributor.png)

output of node-red where we send our colors to the LED strip:

![](media/toggle_on_off_with_colors.png)

Sensors/Actors connected to the Arduinos:

![](media/project_1_2_button.jpg)

![](media/project_1_2_lights.jpg)

And the Python projects, we put on the Raspberry Pi: (Note: we first played around on the command line itself with the UlnoIoT, but then decided to switch to Python, as it has Syntax highlighting and better autocomplete)

Button:
![](media/project1_2_ravecontrol.png)

Onboard LED + LED Strip:
![](media/project1_2_ravelight.png)


###Problems:
The Arduino with the ulnoiot installed needs quite a lot of time(sometimes more than 2 minutes) to startup/boot, which caused us to think that it was not working and so we thought we made a mistake, which we fortunately did not. Node-red also had some strange problems, where we were not able to trigger the onboard LED and the LED strip at the same time with just one output of a function, and on the next day, we simply pressed deploy and it worked flawlessly.
