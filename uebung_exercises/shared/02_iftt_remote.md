#IFTT remote button trigger

##Hardware
Connected button to the arduino
![](media/connected_button.jpg)

Tested the button by pressing it, which triggers a test LED
![](media/button_press_test.jpg)

##Software
###Test Arduinos WLAN connection
Load the http client example from File > Examples > ESP8266HTTPClient > BasicHTTPCLient
Change the WIFI credentials for the arduino to connect into the WLAN of the Raspberry Pi
Changed the url to which to send a test GET request to http://www.example.com/

Note:
We experienced some problems with retrieving the example.com html page. The solution was the "/" at the end of the url.

###Create a IFTTT Webhook
Go to https://ifttt.com/my_applets and create a new applet

![](media/ifttt_webhook.png)

Extract the endpoint of the webhook, which will then send a notification to the IFTTT application

![](media/ifttt_endpoint.png)

###Combine it all
Source code can be found [here](sourcecode/lab2/HttpClient/HttpClient.ino).

#Connecting to the LC Display
##Driver
To get the LCD Display running first, one has to install the corresponding [driver](https://github.com/agnunez/ESP8266-I2C-LCD1602)

##Hardware
Connect the LC Display to the Arduino using the LCD's I2C bus, ground and the 5V energy supply of the Arduino.
 ![](media/lcd_I2C_pins.jpg)

**Note:** You need to use the 5V since the 3.3V energy supply pin of the Arduino is not enought to power the LCD's backlight panel.

![](media/lcd_connected.jpg)

##Software
The usage of the LCD driver library in connection with the sending of a push notification can be found [here](sourcecode/lab2/HttpClientLCD/HttpClientLCD.ino).

