#MQTT With Node-RED
First of all download and install Node-Red from [here](https://www.npmjs.com/package/node-red). Install onto the PI following this instructions:

~~~
sudo npm install -g node-red

node-red

Open http://localhost:1880
~~~

Run the Node-Red server with the following command:

~~~
node-red
~~~

![](media/node_red_terminal.png)

Create a new node-red flow using the webinterface at your servers ip and port:

![](media/node_red_relay.png)

Deploy this flow onto the node-red server and observe the output
![](media/MQTT_server_out.png)

![](media/node_red_debug_out.png)

Received in the MQTTReceiver in Java

![](media/mqtt_receiver_state.png)

![](media/mqtt_receiver_state1.png)

![](media/java_integrator_out.png)

#MongooseOS
Install MongooseOS from [here](https://mongoose-os.com/software.html).

For a MAC (like in my case follow this instructions instead)

~~~
curl -fsSL https://mongoose-os.com/downloads/mos/install.sh | /bin/bash
~/.mos/bin/mos --help      
~/.mos/bin/mos
~~~

access the ui using

~~~
~/.mos/bin/mos ui
~~~

configure your wemos arduino mini
![](media/mongoose_setup.png)

access led and make it blink

![](media/mongoose_tutorial.png)

###Problems
Integrating Libraries is quite difficult, which caused us to waste several hours on mongoose. We decided to switch back to Arduino ulnoiot.
Mongoose also crashed several times and you weren´t able to create projects and/or add files to the project.