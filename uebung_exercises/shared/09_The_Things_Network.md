#The Things Network
##Setup the PI

<img src="media/Lora_GPS_HAT.jpg">

First we started by logging in to our Raspberry Pi and cloned the software for the Things Network from the repo.

```git clone https://github.com/tftelkamp/single_chan_pkt_fwd```

Then we used the RaspberryConfig to enable SPI.

```sudo raspi-config```

- select the Interface options to configure connections to peripherals,
-  select P4 SPI to Enable/Disalbe automatic loading of SPI kernel module
- Would you like the SPI interface to be enabled, say "YES"
- After that, reboot the Raspberry Pi ("sudo shutdown -r now")

Now install wiringPi
```sudo apt-get install wiringpi```

After installing it we configure the single channel gateway:
```
cd ~/single_chan_pkt_fwd
nano main.cpp
```
Now we have to change the IP-Address of the SERVER1 to the european one which is
IP from TTN EU Server: "52.169.76.203"

Now we have to get the gateway ID, which we get by running the following:
```sudo /home/pi/single_chan_pkt_fwd/single_chan_pkt_fwd```

<img src="media/pi_loran_config.png">

Now we copied the gateway ID, which we will used in the console of TheThingsNetwork.

We logged in to the "console.thethingsnetwork.org" and selected Gateways which we registered. Now we entered the gateway ID and Registered the gateway.

Last but not least run the gateway
```sudo /home/pi/single_chan_pkt_fwd/single_chan_pkt_fwd```

The TTN_Gateway:
![](media/ttn_gateway.png)

The TTN_Gateway_Config:
![](media/ttn_gateway_config.png)

The TTN_Sender:
Follwing the instructions from [here](http://wiki.dragino.com/index.php?title=Connect_to_TTN#Use_LoRa_GPS_HAT_and_RPi_3_as_LoRa_End_Device).
![](media/ttn_sender_overview.png)

Receiving data from the sender:
![](media/ttn_sender.jpeg)
