#come up with 2 potentially useful IoT scenarios

##Smart Teepot
People in a Household: Mike, Matt and Lorenz

Everyone of these people loves to drink tea, but everyone wants them in a different intensity and/or flavour. Lorenz likes to only dip his teabag in hot water for about three minutes. Mike prefers heating his tea for about five minutes. Matt is a weird ass guy so he likes to have his tea in the warm water for 10 minutes.

Matt hasn’t been showering since four days. His smell is so intense that the other people in the household want to spend the least time as possible in his direct vicinity.

Luckily everyone's room is connected by multiple Amazon Echo Dots. Simply by saying “Alexa, please make me a cup of tea”, Alexa recognizes the voice of Lorenz and makes him a cup of tea to his preference using the TeaPotMaster-3000. Thirty minutes after that mike wants to get a cup of tea. Again he commands Alexa “Alexa, please make me a cup of tea”. Using voice recognition Alexa again knows Mike’s preference.


##Hotel shower time
People currently living in hotel: Matthias, Lorenz, Patrick

Theese 3 people are currently living in the Internet of Things Hotel in Hagenberg, which is one of the most up to date hotels using voice recognition and fingerprinting for its customers. To still make the hotel affordable, there is only one bathroom for every 3 rooms. Before you can check in online, you can choose different types of showers, there are a lot of different shower heads availiable in the shower. For example a Rain shower head, a lot of shower heads from the sides and even a shower head coming from the floor.
By checking in online, you first have to take a small survey to record your voice via your microphone. Then you can choose between all thoose showerheads before you even enter the hotel. 

Once you are in the hotel and want to take a shower, you simply go into the shower and say:"Start Shower" and your choosen shower starts with the predefined temperature and the predefined shower heads. The same thing works with the fingerprint you have to make when you enter the hotel for the first time, which whom you can open your hotel room and your safe, which is located in your room, for extra safety