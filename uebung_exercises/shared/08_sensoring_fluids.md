#Project 2 sensoring fluids
##Raindrop and acoustic distance sensor

<img src="media/project2_sensoring_node_overview.jpg" width=50%>

The raindrop and acoustic distance node measures these values using the sunfounder raindrop and acoustic distance (HC-SR04) sensors.

The sourcecode of this node can be found [here](../../projects/project2/sensoringNode/sensoringNode.ino).

The sensoring node sends its readings to the node red integrator:
<img src="media/project2_integrator.png">

The integrator buffers the last five distance sensor readings in the resultBuffer function:

<img src="media/project2_sensoring_node_buffer.png" width = 50%>

A lowpassfilter is then applied to the five buffered distance values:

<img src="media/project2_sensoring_node_lpf.png" width = 50%>

Before forwarding the results to the controller, the median over the five distance readings is calculated:

<img src="media/project2_sensoring_node_median.png" width = 50%>

Why are we doing it this way?

<img src="media/project2_sensoring_node_distance_spike.png" height = 300px>

The acoustic distance sensor sometimes spikes its readings for no obvious reason. This can too be observed, when putting your hand on it (simulating a distance of < 1 cm).

We had some problems with The rain sensor which is quite strange, because it is either dry(0) or fully flooded(1023). We now just differentiate with either there is rain or there is not.

In the image above the distance is reduced from 15cm to < 1cm and when reaching < 1cm the distance spikes to 3425cm.
By performing the above tasks, the output sent to the results controller are pretty good:


<img src="media/project2_sensoring_node_distance_clean.png" height = 300px>

The exactly same readings, but at the end of the integrator

##Optic distance sensor

The optic distance node measures these distance using the Adafruit_VL53L0X Distance Sensor.

The sourcecode of this node can be found [here](../../projects/project2/sensoringNode/LightDistanceSensor.ino).

The sensoring node sends its readings to the node red integrator:
<img src="media/Light_sensor_project2.jpg">

<img src="media/project2_integrator.png">

The distance which gets sent by the sensor, travels through clear water and measures the distance from the sensor to the bottom of the water bottle.

The distance sensor calculates the distance kind of inconsistent, even if the sensor is not moved at all. In the Image below you can see that the sensor jumps around from 7,4 - 8,3 centimeters without moving it. The maximum range is about 40 cm, when there is a clear sight to the wall and no uneven obstacles in between, because the sensor spreads out in a kind of a wedge-shaped way.

<img src="media/project2_sensoring_node_distance_light.png">

##Sensor readings
Acoustic distance sensor

- Water: 4.5cm distance -> 4 cm reading, 7.5 cm distance -> 7 cm reading
- Oil: 4.5 cm distance -> 4 cm reading, 7.5 cm distance -> 7 cm reading
- Cola: 4.5 cm distance -> 4 cm reading, 7.5 cm distance -> 7 cm reading

Optic distance sensor

- Water: 6 cm distance ->  10cm reading, 11cm distance -> 14cm distance (less fluid)
- Oil: 6 cm distance ->  8 cm reading, 11cm distance -> 12 cm distance (less fluid)
- Cola: 6 cm distance ->  8 cm reading, 11cm distance -> 13 cm distance (less fluid)

Rain sensor

- Water: pretty bad, having a range from 0 (rain) to 1024 (no rain) it only indicates either 0 or 1024.
- Oil: same as water
- Cola: same as water

##Scale
First of all the scale has to be assembled. A kit from "Keystudio" enables easy assembly and accurate performance. A problem occured since the scales weight sensor was broken on our device. We worked around this by using a different, pre-built scale.

<img src="media/scale.jpg" width=50%>

Next up the scale had to be calibrated. This can be done by installing the HX711 library in the arduino library manager. Now the scale can be calibrated using the serial console and commands provided by the example code. The resulting calibration value for our specific scale was 455.

<img src="media/scale_calibration_value.PNG" width=100%>

Now the scale arduino code has to be extended to send I²C data over to the wemos, which is Wifi-capable to send the measured information via MQTT. To do so the library from the ulnoiot github needs to be implemented and the following code needs to be added.

<img src="media/i2c_code.PNG" width=50%>

Next up is the wemos setup. This board runs ulnoiot for this project. After upgrading to the latest version of ulnoiot the target behaviour can be acquired by the single line implementation seen below.

<img src="media/ulno_scale.PNG" width=50%>

Here the results on the MQTT server can be seen. New data is only sent when the value on the scale changes. The command to listen for MQTT events is "mqtt_all".

<img src="media/mqtt_scale.PNG" width=30%>
