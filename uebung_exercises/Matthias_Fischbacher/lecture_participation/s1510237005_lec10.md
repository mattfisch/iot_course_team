#Lecture notes - 30.10.2017
##IOT startups

###Name successful IoT business ideas
**IFTTT** Helps various apps and devices to work together to produce better results

**BluFlux** Deals with advanced antenna, cellular wearable, indoor real time location systems, low power radar, etc.
Google’s Advanced Technologies and Products has selected BluFlux as their design partners

**Big Belly** http://bigbelly.com/ => Smart City Solutions
Create products like a trash compactor bin which sends notifications to the right officials when it is full

**August**
http://august.com/ => Smart Locks
Remotely lock and unlock doors in your home, keyless entry for family and friends.  

**Adhere Tech**
https://adheretech.com/ => Smart wireless pill bottles
If a patient does not take his/her pills, a notification or alert gets sent to neighbours, family, doctors etc.

**Humavox**
http://www.humavox.com/ => Wireless charging of small IoT-devices

###Name failed IoT business ideas
**Sigmo**
https://www.crowdfundinsider.com/2015/05/67907-where-are-they-now-sigmo-fails-to-talk-understand/  
A voice translating device that promised “at the touch of a button” you would be able to instantly translate the words you need into any chosen language and hear the results of your translation.   
Unfortunately for backers of this project the only thing ever delivered were broken promises and pretty pictures.  
Sigmo is a crowdfunding failure.

**Ninja Blocks**
https://www.crunchbase.com/organization/ninja-blocks  
Ninja Blocks is the creator of the Ninja Sphere, the missing brain for your smart devices. Ninja Sphere learns from your behaviour, identifies your preferences, and manages your devices so you don't have to.  
The Ninja Sphere incorporates a gesture interface and micro-location technology that accurately locates radios within the home. It is backwards compatible with the Ninja Block, a popular device and platform that supports 100s of third-party devices.  
Ninja's should be effective and invisible as should the interface to your home
Failed:
http://www.linux-magazin.de/NEWS/Ninja-Blocks-Projekt-ist-pleite
Because getting broke while producing devices.

**Narative**
https://www.crunchbase.com/organization/narrative  
Narrative provides hardware and software tools for automatic visual storytelling and known for the wearable camera narrative clip.
=>http://getnarrative.com/

**Zeo**
https://www.crunchbase.com/organization/zeo  
Zeo manufactures sleep monitors that record electrical activity along the scalp and monitor the user's brain waves.  
Failed: http://www.mobihealthnews.com/22484/take-positive-lessons-from-zeo-failure   
The main problem was the business model, with a suboptimal profit margin; also, users didn't care much about accuracy, and competing devices like the FitBit, based on (less accurate) actigraphy, were apparently "good enough" while also performing other functions like monitoring physical activity.

###What are challenges and opportunities arising from IoT for small businesses

####Opportunities
- Huge unclaimed market
- Interrest and optimism of business and consumers high
- Many companys supporting IoT comming up (Thinger, TheThingsNetwork, Mongoose ...)
- Better opportunities for startups than for big companies, since they can adapt to market needs faster.
- Cost effective hard- and software solutions for developers
- India is developing into a lucrative market because of the burst in mobile phone owners

####Challenges
- Slow development of market and tech
- Big competition
- Technological challenges: Security, Connectivity, Compatibility & Longevity, Standards
- Customer demands and requirements change constantly
- Lack of understanding or education by consumers of best practices for IoT devices security to help in improving privacy, for example change default passwords of IoT devices.
- IoT is complex

###Discuss the potential of your project three to be turned into a business venture

##Interview preperation
###Three IoT domains they are influencing
Encryption, Decryption, Security, Business, Communication

###Projects they were involved in
- Aisencryp
- IDNumgen
- Acctgui

###Current projects they are involved in
- Partner of Samsung, Seat, Intel and Amazon AWS IoT
- SSL working on low power platforms
- Monero, Crypto Currency

###Two points on their impact on startups
- Security related improvements
- Secure element

###One quesion
