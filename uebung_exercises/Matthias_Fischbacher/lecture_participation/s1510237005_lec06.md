#Lecture notes - 17.10.2017

##UlnoIoT questions

* How to handle connectivity issues with the arduino
* How to perform a remote flash
* Animations for the LED strip

##Display

control display via MQTT messages

##Analog demo

    analog("water", threshold=100, precision=5)
    analog("water", precision=5) // analog sensor
    analog("water", precision=5, threshold= 500) // digital sensor
    
##Build a driver
    cd("ulnoiot")

    import gc
    # free memory
    gc.mem_free()
    # collect some garbage
    gc.collect()
    # derive from a Device class
    class Display(Device):
    # copy new_device.py and fill in the wanted values
