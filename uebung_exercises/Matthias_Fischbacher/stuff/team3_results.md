#Sensing/Acting
##Rain Water Sensor
The rain water sensor has a dry working voltage of 5V. As rainwater falls onto it, the exposed wires are bridged and the output gradually becomes less. It is an analog sensor, and the amount/strength of the rain can be determined by analysing the analog output. 
Wiring: The sensor needs 5V of power to work, the + pin is connected to the corresponding + pin on the sensor board, and the – pin is connected to the corresponding – pin on the sensor board. Needs A/D Converter

- analog signal with value
	* 0mm - 480
	* 5mm - 530
	* 10mm - 615
	* 15mm - 660
	* 20mm - 680
	* 25mm - 690
	* 30mm - 700
	* 35mm - 705
	* 40mm - 71

##HX711
Precision 24-bit analogto-digital converter (ADC) designed for weighscales and industrial control applications to interface directly with a bridge sensor. 
Two selectable differential input channels
On-chip active low noise PGA with selectable gain of 32, 64 and 128
On-chip power supply regulator for load-cell and ADC analog power supply 
Digital control and serial interface: pin-driven controls, no programming needed
Operation supply voltage range: 2.6 ~ 5.5V
Channel A can be programmed with a gain of 128 or 64, corresponding to a full-scale differential input voltage of ±20mV or ±40mV when 5V supplied on AVDD

Pin description

- 1 VSUP Power Regulator supply: 2.7 ~ 5.5V
- 2 BASE Analog Output Regulator control output（NC when not used）
- 3 AVDD Power Analog supply: 2.6 ~ 5.5V
- 4 VFB Analog Input Regulator control input（connect to AGND when not used）
- 5 AGND Ground Analog Ground
- 6 VBG Analog Output Reference bypass output
- 7 INA- Analog Input Channel A negative input
- 8 INA+ Analog Input Channel A positive input
- 9 INB- Analog Input Channel B negative input
- 10 INB+ Analog Input Channel B positive input
- 11 PD_SCK Digital Input Power down control (high active) and serial clock input
- 12 DOUT Digital Output Serial data output
- 13 XO Digital I/O Crystal I/O (NC when not used）
- 14 XI Digital Input Crystal I/O or external clock input, 0: use on-chip oscillator
- 15 RATE Digital Input Output data rate control, 0: 10Hz; 1: 80Hz
- 16 DVDD Power Digital supply: 2.6 ~ 5.5V 

Pin PD_SCK and DOUT are used for data retrieval, input selection, gain selection and power down controls. 

Output
The output 24 bits of data is in 2’s complement format. When input differential signal goes out of
the 24 bit range, the output data will be saturated at 800000h (MIN) or 7FFFFFh (MAX), until the
input signal comes back to the input range

##Optical Distance VL53L0X

## Acoustic Distance HC-SR05
###How to wire to GPIO/Analog port(s)
####HC-SR05 PINS
The HC-SR05 has 5 pins (left to right):

* VCC
* Trig
* Echo
* GND

####Wiring to a raspberry pi
There are four pins on the ultrasound module that are connected to the Raspberry:

* VCC to Pin 2 (VCC)
* GND to Pin 6 (GND)
* TRIG to Pin 12 (GPIO18)
* Connect the 330Ω resistor to ECHO.  On its end you connect it to Pin 18 (GPIO24) and through a 470Ω resistor you connect it also to Pin6 (GND).

We do this because the GPIO pins only tolerate maximal 3.3V. The connection to GND is to have a obvious signal on GPIO24. If no pulse is sent, the signal is 0 (through the connection with GND), else it is 1. If there would be no connection to GND, the input would be undefined if no signal is sent (randomly 0 or 1), so ambiguous.

![](https://tutorials-raspberrypi.de/wp-content/uploads/2014/05/ultraschall_Steckplatine.png)

###Measure distance
Send a 10 uS wide pulse to the sensor on the Trigger Pin. 

The sensor will automatically send out a 40 kHz wave.

Begin monitoring the output from the Echo Pin and
when the Echo Pin goes high, begin a timer.

When the Echo Pin goes low, record the **elapsed time** from the timer and use the following conversion formula:

Distance (in cm) = (**elapsed time** * sound velocity) / 100 / 2

Note: 

* Divide distance by 2 because the sensor returns the round trip time, which doubles the distance measurement.
* sound velocity(340 m/s)