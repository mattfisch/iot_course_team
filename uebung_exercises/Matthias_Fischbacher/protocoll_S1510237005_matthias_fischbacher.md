#IOT protocol
##Participation proofs:
Final presentation points are included into participation proof 12

- Lecture [one](lecture_participation/S1510237005_lec01.md)
- Lecture [two](lecture_participation/S1510237005_lec02.md)
- Lecture [three](lecture_participation/S1510237005_lec03.md)
- Lecture [four](lecture_participation/S1510237005_lec04.md)
- Lecture [five](lecture_participation/S1510237005_lec05.md)
- Lecture [six](lecture_participation/S1510237005_lec06.md)
- Lecture [seven](lecture_participation/S1510237005_lec07.md)
- Lecture [eight](lecture_participation/S1510237005_lec08.md)
- Lecture [nine](lecture_participation/S1510237005_lec09.md)
- Lecture [ten](lecture_participation/S1510237005_lec10.md)
- Lecture [onetyone](lecture_participation/S1510237005_lec11.md)
- Lecture [onetytwo](lecture_participation/S1510237005_lec12.md)

##Lab 1 - 02.10.2017
###Setup of the Raspberry Pi and the miniOS on a SD card:

[protocol](../shared/01_io_miniOS_setup.md)

My team colleague and I setup the Raspberry Pi together and flashed some SD cards for our colleagues.

###Remotely toggle the LED on the Arduino:

[protocol](../shared/03_remote_led_ping.md)

Using Pair programming with my team colleague we wrote the LedServer.ino file and flashed it onto the Arduino.

##Lab 2 - 03.10.2017

[protocol](../shared/02_iftt_remote.md)

Each of us created an IFTTT webhook. Using pair programming we coded up the HttpClient.ino and HttpClientLCD.ino files (Which both send a notification to each of our phones when the button attached to the Arduino is pressed) and flashed them onto the Arduino.

For the user stories see: [user stories](../shared/04_user_stories.md)

##Lab 3 - 11.10.2017

[protocol](../shared/06_NodeRed_MongooseOS.md)

We learned how to use an integrator as controlling part of an IoT environment. We tried out Node-Red as integrator and created a simple hello world program. We also tested out another IDE than Arduino Studio. Mongoose is a web based IDE, which supports C and JavaScript code to be run on the wemos d1. We started our work on project1_1.

##Lab 4 - 16.10.2017
In this exercise we had to rebuild project 1 with the ulnoiot framework.
This was done by my colleague Patrick and me. While I ported the project, Patrick protocolled our work and problems with the ulnoiot framework here:

[protocol](../shared/07_Rebuild_Project1_With_UlnoIoT.md)

##Lab 5, 6 - 17.10.2017 and 23.10.2017
During lab five and six I created the sensoring node (including the Node-Red integrator), which performs rain and acoustic distance measuring as described in the protocoll of project 2 found here:

[protocol](../shared/08_sensoring_fluids.md)


Patrick created the sensoring node for optical distance measurement and Lorenz assembled and connected the scale.

##Lab 7 - 24.10.2017
At lab 7 we created a LORAN sender and receiver, the protocol for this can be found here:

[protocol](../shared/09_The_Things_Network.md)

We followed the instructions of the links in the exercises and attached the shield, with which LORAN signals are received from LORAN senders and sent to the TTN page, where we created an account and registered the gateway.

##Lab 8 - 25.10.2017
During lab 8 we started with the project three and presented our results from project two.

Results from project two: [protocol](../shared/08_sensoring_fluids.md)

The documentation of project three can be found here: [protocol](../../projects/project3/project_03_scenarios.md)

We came up with the idea and wrote down a user story for the project three. Additionally the team size changed from three to five, our two colleagues Michael and Martin joined us for the last project.

##Lab 9 - 27.10.2017
In lab 9 we created the mobile application for the project three and started with the toilet paper weighting. Lorenz created and hosted the backend, which stores the informations about toilet paper and the shower statistics on his webserver.

- The documentation for the backend Lorenz made can be found here:
	* sourcecode:
	* documentation: [backend](../../projects/project3/project_03_backend.md)
	* API documentation: [api documentation](../../projects/project3/api_documentation.md)
- The protocol of the mobile application made by Lorenz, Michael and me can be found here:
	* documentation: [app](../../projects/project3/project_03_ionic_app.md)
	* source code: [ionic app](../../projects/project3/app/SSC)
- The protocol of the toiletpaper node Patrick made can be found here:
	* documetation: [toilet paper counter](../../projects/project3/project_03_toiletpaper_counter.md)
	* source code: [toilet paper counter](../../projects/project3/ToiletPaperCounter/ToiletPaperCounter.ino)
- The protocol of the showerstats node Martin can be found here:
	* documetation: [shower control](../../projects/project3/project_03_showerctrl.md)
	* source code: [shower control](../../projects/project3/ShowerCtrl/ShowerCtrl.ino)
- The protocol of the toilet-in-use node Michael made can be found here:
	* documetation: [reed switch](../../projects/project3/project_03_reedswitch.md)
	* source code: [reed switch](../../projects/project3/reed_switch)
- The protocol of the Node-Red integrator made by Patrick and me can be found here:
	* documetation: [integrator](../../projects/project3/project_03_nodered.md)

##Lab 10 - 30.10.2017
Continuing our work from lab 9.

##Lab 11 - 31.10.2017
Made the final presentations, including protocolls and the pitch, storyboard and [plot](../../projects/project3/project_03_presentation_plot.md) for the video. The video can be found on youtube: [Flatty ssc](https://www.youtube.com/watch?v=GDgvd_f9TS8)
