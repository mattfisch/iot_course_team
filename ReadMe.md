#ReadMe

Hello and welcome to the Repository for the course Internet of Things at the FH Hagenberg.
We are Matthias Fischbacher, Lorenz Graf and Patrick Felbauer:

[Fischbacher](uebung_exercises/Matthias_Fischbacher/protocoll_S1510237005_matthias_fischbacher.md)

[Graf](uebung_exercises/Lorenz_Graf/protocoll_S1510237007_lorenz_graf.md)

[Felbauer](uebung_exercises/Patrick_Felbauer/protocol_s1510237003_felbauer.md)

Thanks for visiting our Repository.